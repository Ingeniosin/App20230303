package me.juan.learning.calculator;

import lombok.Getter;
import me.juan.learning.historical.HistoricalManager;

@Getter
public class CasioCalculator extends GenericCalculator {
    private final HistoricalManager historicalManager;

    public CasioCalculator(String username) {
        super("Casio", "FX-991ES PLUS");
        this.historicalManager = new HistoricalManager(this, username);
    }

    @Override
    public double add(double a, double b) {
        return historicalManager.addHistory(a, b, super.add(a, b), Operation.ADDITION);
    }

    @Override
    public double subtract(double a, double b) {
        return historicalManager.addHistory(a, b, super.subtract(a, b), Operation.SUBTRACTION);
    }

    @Override
    public double multiply(double a, double b) {
        return historicalManager.addHistory(a, b, super.multiply(a, b), Operation.MULTIPLICATION);
    }

    @Override
    public double divide(double a, double b) {
        return historicalManager.addHistory(a, b, super.divide(a, b), Operation.DIVISION);
    }

    @Override
    public double power(double a, double b) {
        return historicalManager.addHistory(a, b, super.power(a, b), Operation.POWER);
    }
}
