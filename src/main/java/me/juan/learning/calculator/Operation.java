package me.juan.learning.calculator;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Operation {
    ADDITION("Suma", "+"),
    SUBTRACTION("Resta", "-"),
    MULTIPLICATION("Multiplicación", "*"),
    DIVISION("División", "/"),
    POWER("Potencia", "^");

    private String name;
    private String symbol;
}
